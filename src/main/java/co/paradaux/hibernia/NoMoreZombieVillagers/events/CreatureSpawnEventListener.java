package co.paradaux.hibernia.NoMoreZombieVillagers.events;

import org.bukkit.entity.EntityType;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.plugin.Plugin;

public class CreatureSpawnEventListener implements Listener {

    Plugin plugin;

    public CreatureSpawnEventListener(Plugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onSpawn(CreatureSpawnEvent e) {
        if (e.getEntity().getType() == EntityType.ZOMBIE_VILLAGER) {
            e.setCancelled(true);
        }
    }

}
